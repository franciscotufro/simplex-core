#include <Simplex/Core/Exception.h>
#include <Simplex/Core/Logger.h>
namespace Simplex
{
  namespace Core
  {
    
    Exception::Exception ()
    {
      this->SetDescription ( "Generic Exception" );
    }
    
    Exception::Exception ( std::string description )
    {
      this->SetDescription ( description );
    }
    
    void Exception::NullCheck ( void * pointer, Exception& exception )
    {
      if ( pointer == NULL)
      {
        Throw(exception);
      }
    }

    std::string Exception::GetDescription ()
    {
      return mDescription;
    }
    
    void Exception::SetDescription ( std::string description )
    {
      this->mDescription = description;
    }
    
    std::ostream& operator<< ( std::ostream& os, Exception& exception )
    {
      os << exception.GetDescription();
      return os;
    }
    
    void Exception::Throw( Exception& exception)
    {
        std::cout << "Error: " << exception << std::endl;
        throw exception;
    }
  }
}