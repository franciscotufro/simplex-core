#include <Simplex/Core/SceneGraph.h>
#include <Simplex/Core/Logger.h>

namespace Simplex
{

  namespace Core
  {

    SceneGraph::SceneGraph()
    {
      mSceneGraphTree = new Tree();
      mCurrentTransform = Matrix4 ();
      mStarted = false;
    }

    SceneGraph::~SceneGraph ()
    {
      delete mSceneGraphTree;
    }

    void SceneGraph::AddSceneObject ( SceneObject* object ) 
    { 
      mSceneGraphTree->AddNode( (Node*) object );

      if ( HasStarted () )
        object->StartCallback ();
    }

    void SceneGraph::RemoveSceneObject ( SceneObject* object ) 
    { 
      mSceneGraphTree->RemoveNode( (Node*) object );
    }
        
    bool SceneGraph::HasSceneObject ( SceneObject* object ) 
    {
      return mSceneGraphTree->HasNode( (Node*) object ); 
    }

    bool SceneGraph::HasStarted ()
    {
      return mStarted;
    }

    void SceneGraph::Start ()
    {
      mStarted = true;

      Node* root = mSceneGraphTree->GetRootNode ();
      StartChildrenFor ( root );
    }

    void SceneGraph::Start ( SceneObject* object )
    {
      object->StartCallback();
      StartChildrenFor ( object );
    }

    void SceneGraph::StartChildrenFor ( Node* object )
    {
      for ( int i = 0; i < object->ChildCount (); i++ )
      {
        Start ( (SceneObject*)object->GetChildAt ( i ) );
      }
    }

    void SceneGraph::Stop ()
    {
      mStarted = false;
    }

    void SceneGraph::Update ()
    {
      Node* root = mSceneGraphTree->GetRootNode ();
      UpdateChildrenFor ( root );
    }
    
    void SceneGraph::Update ( SceneObject* object )
    {
      object->UpdateCallback ();
      UpdateChildrenFor ( object );
    }

    void SceneGraph::UpdateChildrenFor ( Node* object )
    {
      for ( int i = 0; i < object->ChildCount (); i++ )
      {
        this->Update ( (SceneObject*)object->GetChildAt ( i ) );
      }
    }
    
    void SceneGraph::Render ()
    {
      mCurrentTransform = Matrix4::IDENTITY;
      Node* root = mSceneGraphTree->GetRootNode ();
      RenderChildrenFor ( root );
    }
    
    void SceneGraph::Render ( SceneObject* object )
    {
      CalculateCurrentTransform ( object );
      object->RenderCallback ( &mCurrentTransform );
      RenderChildrenFor ( object );
      RollbackCurrentTransform ( object );
    }

    void SceneGraph::RenderChildrenFor ( Node* object )
    {
      for ( int i = 0; i < object->ChildCount (); i++ )
      {
        this->Render ( (SceneObject*)object->GetChildAt ( i ) );
      }
    }
    
    void SceneGraph::CalculateCurrentTransform ( SceneObject* object )
    {
      mCurrentTransform = mCurrentTransform.concatenate ( object->GetLocalTransform () );
    }
    
    void SceneGraph::RollbackCurrentTransform ( SceneObject* object )
    {
      mCurrentTransform = mCurrentTransform.concatenate ( object->GetLocalInverseTransform () );
    }
    
  }
  
}
