#include <Simplex/Core/Tree.h>

namespace Simplex
{

  namespace Core
  {

    Tree::Tree()
    {
      mRootNode = new Node ();
    }

    Tree::~Tree ()
    {
      delete mRootNode;
    }

    void Tree::AddNode ( Node* node ) 
    { 
      mNodeRegistry.push_back ( node );
      
      if ( !node->HasParent () )
      {
        node->SetParent ( mRootNode );
      }
      
    }

    void Tree::RemoveNode ( Node* node ) 
    { 
        RemoveNodeFromRegistry ( node );
        node->RemoveParent();
    }
    
    void Tree::RemoveNodeFromRegistry ( Node* node )
    {
      Tree::NodeRegistryIterator index = FindObjectInRegistry ( node );
      mNodeRegistry.erase ( index );
    }
    
    Tree::NodeRegistryIterator Tree::FindObjectInRegistry ( Node* node )
    {
      for ( NodeRegistryIterator i = mNodeRegistry.begin () ; i < mNodeRegistry.end (); i++ )
      {
        if ( *i == node )
        {
          return i;
        }
      }
      throw Exception("Object not found.");
    }
    
    bool Tree::HasNode ( Node* _node ) 
    { 
      for ( int i = 0; i < mNodeRegistry.size (); i++ )
      {
        if ( mNodeRegistry[i] == _node )
          return true;
      }
      return false;
    }
    
    Node* Tree::GetRootNode ()
    {
      return mRootNode;
    }
    
  }
  
}
