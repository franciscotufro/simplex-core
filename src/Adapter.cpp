#include <Simplex/Core/Adapter.h>

namespace Simplex
{

  namespace Core
  {
    
    Adapter::Adapter()
    {
      mAdaptee = 0;
    }
    
    Adapter::~Adapter()
    {
      delete mAdaptee;
    }
    
    void Adapter::SetAdaptee ( Adaptee* adaptee )
    {
      mAdaptee = adaptee;
    }
    
    Adaptee* Adapter::GetAdaptee ()
    {
      return mAdaptee;
    }
    
    bool Adapter::HasAdaptee ()
    {
      return !(mAdaptee == 0);
    }
  }
  
}