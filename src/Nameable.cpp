#include <Simplex/Core/Nameable.h>

namespace Simplex
{

  namespace Core
  {
        
    std::string const Nameable::GetName()
    {
      return mName;
    }

    void Nameable::SetName ( std::string name )
    {
      mName = name;
    }
       
  }
  
}