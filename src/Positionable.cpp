#include <Simplex/Core/Positionable.h>

using namespace Simplex::Math;

namespace Simplex
{

  namespace Core
  {
    
    Positionable::Positionable()
    {
      InitializePostion();
      InitializeScale();
      InitializeRotation();
    }
    
    Positionable::~Positionable ()
    {
    }
        
    Math::Vector3 const Positionable::GetPosition() 
    { 
      return mPosition; 
    }
    
    void Positionable::SetPosition ( Math::Vector3 const _position ) 
    {
      mPosition = _position; 
    }

    Math::Vector3 const Positionable::GetScale() 
    { 
      return mScale; 
    }
    
    void Positionable::SetScale ( Math::Vector3 const _scale ) 
    {  
      mScale = _scale; 
    }

    Math::Quaternion const Positionable::GetRotation() 
    { 
      return mRotation; 
    }
    
    void Positionable::SetRotation (Math::Quaternion const _rotation) 
    {  
      mRotation = _rotation; 
    }
    
    Matrix4 Positionable::GetLocalTransform ()
    {
      Matrix4 m;
      m.makeTransform ( mPosition, mScale, mRotation );
      return m;
    }

    Matrix4 Positionable::GetLocalInverseTransform ()
    {
      Matrix4 m;
      m.makeInverseTransform ( mPosition, mScale, mRotation );
      return m;
    }
    
    
    //
    // Auxiliary Functions
    //
    void Positionable::InitializePostion()
    {
      SetPosition( Vector3::ZERO );
    }
    
    void Positionable::InitializeScale()
    {
      SetScale ( Vector3::UNIT_SCALE );
    }
    
    void Positionable::InitializeRotation()
    {
      SetRotation ( Quaternion::IDENTITY );
    }
    
  }
  
}