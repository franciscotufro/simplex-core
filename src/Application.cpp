#include <Simplex/Core/Application.h>
#include <stdio.h>

namespace Simplex
{

  namespace Core
  {
    Application::Application ()
    {
	    mSceneGraph = new SceneGraph ();
    }

    Application::~Application ()
    {
      delete mSceneGraph;
    }

    SceneGraph* Application::GetSceneGraph ()
    {
      return mSceneGraph;
    }
  }
}
