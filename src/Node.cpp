#include <Simplex/Core/Node.h>

namespace Simplex
{

  namespace Core
  {
    
    Node::Node()
    {
      InitializeParent ();
    }
    
    Node::~Node ()
    {
      
    }

    bool Node::HasParent()
    {
      return this->mParent != NULL;
    }
    
    Node* Node::GetParent () 
    { 
      ValidateParentNotNull ( this->mParent );
      return this->mParent;
    }
    
    void Node::SetParent ( Node* _parent ) 
    { 
      ValidateParentNotNull ( _parent );

      if( mParent )
        RemoveParent();
        
      this->mParent = _parent; 
      _parent->AddChild ( this );
    }
    
    void Node::ValidateParentNotNull ( Node * parent )
    {
      NullParentException exception = NullParentException("Null parent for Node");
      Exception::NullCheck ( (void*)parent, exception );
    }
    
    bool Node::HasChild ( Node* child )
    {
      for (int i = 0; i < mChildren.size(); ++i)
      {
        if ( mChildren[i] == child )
        {
          return true;
        }
      }
      return false;
    }
    
    Node* Node::GetChildAt ( int index ) 
    { 
      return mChildren[index]; 
    }
    
    void Node::RemoveParent ()
    {
      mParent->RemoveChild ( this );
      this->mParent = NULL;
      
    }
    
    int Node::ChildCount () 
    {
      return this->mChildren.size(); 
    }
    
    void Node::AddChild ( Node* _child ) 
    { 
      mChildren.push_back ( _child ); 
    }

    void Node::RemoveChild ( Node* child ) 
    {
      Node::ChildrenIterator index = FindChild ( child );
      mChildren.erase ( index );
    }
    
    Node::ChildrenIterator Node::FindChild ( Node* object )
    {
      ChildrenIterator i;
      for ( i = mChildren.begin () ; i < mChildren.end (); i++ )
      {
        if ( *i == object )
        {
          break;
        }
      }
      return i;
    }

    void Node::InitializeParent()
    {
      this->mParent = NULL;
    }
    
  }
 
  
}