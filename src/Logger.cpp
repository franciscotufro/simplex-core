#include <Simplex/Core/Logger.h>

namespace Simplex
{
  namespace Core
  {
    Logger* Logger::mLogger = NULL;

    Logger* Logger::Instance()
    {
      return GetExistingLoggerOrCreateOne ();
    }

    Logger* Logger::GetExistingLoggerOrCreateOne () 
    {
      if (!mLogger)
        mLogger = new Logger ();

      return mLogger;
    }

    std::ostream* Logger::GetOutputStream ()
    {
      return GetCurrentOutputStreamOrStdCoutIfNull ();
    }
    
    std::ostream* Logger::GetCurrentOutputStreamOrStdCoutIfNull()
    {
      
      if ( mOutputStream )
        return dynamic_cast<std::ostream*> ( mOutputStream );

      return &std::cout;
    }
    
    void Logger::SetOutputStream ( std::stringstream* stream )
    {
      mOutputStream = stream;
    }
    
    void Logger::ClearOutputStream ()
    {
      mOutputStream = NULL;
    }
    
    void Logger::Log ( std::string message )
    {
      Instance()->InstanceLog ( message );
    }

    void Logger::Log ( int value )
    {
      Instance()->InstanceLog( ConvertIntToString ( value ) );
    }

    void Logger::Log ( float value )
    {
      Instance()->InstanceLog( ConvertFloatToString ( value ) );
    }

    void Logger::Log ( std::vector<char> value )
    {
      std::string str(value.begin(),value.end());
      Instance()->InstanceLog( str );
    }

    
    
    void Logger::InstanceLog ( std::string message )
    {
      *GetOutputStream() << message << std::endl << std::flush;
    }
    
    Logger::Logger ()
    {
      ClearOutputStream ();
    }

    std::string Logger::ConvertIntToString ( int value )
    {
      std::ostringstream temporaryStringBuffer;
      temporaryStringBuffer << value;
      return temporaryStringBuffer.str();
    }

    std::string Logger::ConvertFloatToString ( float value )
    {
      std::ostringstream temporaryStringBuffer;
      temporaryStringBuffer << value;
      return temporaryStringBuffer.str();
    }
      
  }
}