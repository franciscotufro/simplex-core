#ifndef __SIMPLEX_CORE_ADAPTER_H__
#define __SIMPLEX_CORE_ADAPTER_H__

#include <Simplex/Core/Adaptee.h>

namespace Simplex
{
  namespace Core
  {
    class Adapter
    {
      
    public:
      
      void SetAdaptee ( Adaptee* adaptee );
      Adaptee* GetAdaptee();
      bool HasAdaptee ();
      
      Adapter();
      ~Adapter();

    private:
      Adaptee* mAdaptee;
      
    };
  }
}

#endif