#ifndef __SIMPLEX_CORE_APPLICATION_H__
#define __SIMPLEX_CORE_APPLICATION_H__

#include <Simplex/Core/SceneGraph.h>

namespace Simplex
{
  namespace Core
  {
    class Application 
    {
      
    protected:
      SceneGraph* mSceneGraph;

    public:
      Application ();
      ~Application ();
      
      SceneGraph* GetSceneGraph();
      
    };  
  }
}


#endif