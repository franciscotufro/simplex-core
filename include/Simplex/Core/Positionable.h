#ifndef __SIMPLEX_CORE_POSITIONABLE_H__
#define __SIMPLEX_CORE_POSITIONABLE_H__

#include <Simplex/Math/Vector3.h>
#include <Simplex/Math/Matrix4.h>

namespace Simplex
{
  namespace Core
  {
    class Positionable
    {
    public:
      Positionable ();
      ~Positionable ();

      Math::Vector3 const GetPosition();
      void SetPosition ( Math::Vector3 const _position );

      Math::Vector3 const GetScale();
      void SetScale ( Math::Vector3 const _scale );

      Math::Quaternion const GetRotation();
      void SetRotation (Math::Quaternion const _rotation);
      
      Math::Matrix4 GetLocalTransform ();
      Math::Matrix4 GetLocalInverseTransform ();

    private:

      Math::Vector3 mPosition;
      Math::Vector3 mScale;
      Math::Quaternion mRotation;

      // Auxiliary Functions
      void InitializePostion();
      void InitializeScale();
      void InitializeRotation();

    };
  }
}
#endif
