#ifndef __SIMPLEX_CORE_TREE_H__
#define __SIMPLEX_CORE_TREE_H__

#include <Simplex/Core/Node.h>

namespace Simplex
{
  namespace Core
  {
    class Tree
    {
    public:
      typedef Exception ObjectNotFound;
      typedef std::vector<Node*>::iterator NodeRegistryIterator;
      
      Tree ();
      ~Tree ();
      
      void AddNode ( Node* _object );
      void RemoveNode ( Node* _object );
      bool HasNode ( Node* _object );
      
      Node* GetRootNode ();
      
    private:
      
      Node* mRootNode;
      std::vector<Node*> mNodeRegistry;
      
      void RemoveNodeFromRegistry ( Node* _object );
      NodeRegistryIterator FindObjectInRegistry ( Node* object );
      
    };
  }
}

#endif