#ifndef __SIMPLEX_CORE_ASSET_H__
#define __SIMPLEX_CORE_ASSET_H__

#include <Simplex/Core/Nameable.h>

namespace Simplex
{
  namespace Core
  {
  	struct AssetDefinition
  	{
  		std::string name;
  		std::string filename;
  	};

    template < class AssetDefinitionStruct >
    class Asset : public Simplex::Core::Nameable
    {

    public:

    	AssetDefinitionStruct& GetDefinition ()
      {
        return mDefinition;
      }

      void SetDefinition ( AssetDefinitionStruct& definition ) 
      {
        mDefinition = definition;
      }

      virtual void ParseDefinition () {}

    private:
    	AssetDefinitionStruct mDefinition;

    };
  }
}
#endif
