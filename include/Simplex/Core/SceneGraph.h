/*

  SceneGraph
  -----------
  
  The SceneGraph is used to set a scene.

  When an object is added to the graph, it will be updated and rendered
  by the SceneGraph according to the hierarchy defined by parent-child 
  relationships.

*/
#ifndef __SIMPLEX_CORE_SCENE_GRAPH_H__
#define __SIMPLEX_CORE_SCENE_GRAPH_H__

#include <Simplex/Core/Tree.h>
#include <Simplex/Math/Matrix4.h>
#include <Simplex/Core/SceneObject.h>

using namespace Simplex::Math;

namespace Simplex
{
  namespace Core
  {
    class SceneGraph
    {
    public:
      typedef Exception ObjectNotFound;
      typedef std::vector<SceneObject*>::iterator SceneObjectRegistryIterator;
      
      SceneGraph ();
      ~SceneGraph ();
      
      bool HasStarted ();
      void Start ();

      void Stop ();

      void Update ();
      
      void Render ();
      
      void AddSceneObject ( SceneObject* object );
      void RemoveSceneObject ( SceneObject* object );
      bool HasSceneObject ( SceneObject* object );
      
    private:
      Tree* mSceneGraphTree;
      bool mStarted;

      Math::Matrix4 mCurrentTransform;
      
      void CalculateCurrentTransform ( SceneObject* object );
      void RollbackCurrentTransform ( SceneObject* object );
      
      void Start ( SceneObject* object );
      void StartChildrenFor ( Node* object );
      
      void Stop ( SceneObject* object );
      void StopChildrenFor ( Node* object );

      void Update ( SceneObject* object );
      void UpdateChildrenFor ( Node* object );

      void Render ( SceneObject* object );
      void RenderChildrenFor ( Node* object );
      
    };
  }
}

#endif