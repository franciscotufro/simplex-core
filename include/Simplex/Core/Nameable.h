#ifndef __SIMPLEX_CORE_NAMEABLE_H__
#define __SIMPLEX_CORE_NAMEABLE_H__

#include <string>

namespace Simplex
{
  namespace Core
  {
    class Nameable
    {
    public:
      std::string const GetName();
      void SetName ( std::string name );
      
    private:
      std::string mName;
    };
  }
}
#endif
