#ifndef __SIMPLEX_CORE_EXCEPTION_H__
#define __SIMPLEX_CORE_EXCEPTION_H__

#include <string>
#include <exception>

namespace Simplex
{
  namespace Core
  {
    class Exception
    {
    public:
      Exception ();
      Exception ( std::string description );

      static void NullCheck ( void * pointer, Exception& exception );
      std::string GetDescription ();
      
      static void Throw( Exception& e);

      friend std::ostream& operator<<(std::ostream& os, Exception& exception);
      

    private:
      std::string mDescription;

      void SetDescription( std::string description );
      
    };
  }
}
#endif
