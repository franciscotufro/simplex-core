#ifndef __SIMPLEX_CORE_LOGGER_H__
#define __SIMPLEX_CORE_LOGGER_H__

#include <iostream>
#include <sstream>
#include <vector>

namespace Simplex
{
  namespace Core
  {
    class Logger
    {
    public:
      static Logger* Instance ();
      static void Log ( std::string message );
      static void Log ( int value );
      static void Log ( float value );
      static void Log ( std::vector<char> value );

      std::ostream* GetOutputStream ();
      void SetOutputStream ( std::stringstream* stream );
      void ClearOutputStream ();
      
    private:
      void InstanceLog ( std::string message );
      

      static Logger* mLogger;

      std::stringstream* mOutputStream;
      
      Logger ();
      
      /* Auxiliary Methods */
      static Logger* GetExistingLoggerOrCreateOne ();
      std::ostream* GetCurrentOutputStreamOrStdCoutIfNull();
      static std::string ConvertIntToString (int value);
      static std::string ConvertFloatToString (float value);

      
    };
  }
}

#endif