/*

  SceneObject
  -----------
  
  A SceneObject is the core element of the SceneGraph.

  They have position, scale and rotation attributes.

  Parent-child relationships for SceneObjects are used to inherit 
  the parent's attributes.
  The final position of the SceneObject will be calculated recursively,
  multiplying the transforms from all the ancestors from the root to its
  parent. 

*/

#ifndef __SIMPLEX_CORE_OBJECT_H__
#define __SIMPLEX_CORE_OBJECT_H__

#include <Simplex/Core/Node.h>
#include <Simplex/Core/Nameable.h>
#include <Simplex/Core/Positionable.h>
#include <Simplex/Math/Vector3.h>
#include <Simplex/Math/Quaternion.h>
#include <Simplex/Math/Matrix4.h>
#include <Simplex/Core/Exception.h>

#include <string>
#include <vector>

namespace Simplex
{
  namespace Core
  {
    class SceneObject : 
      public Simplex::Core::Node, 
      public Simplex::Core::Nameable, 
      public Simplex::Core::Positionable
    {
    public:
      typedef std::vector<SceneObject*>::iterator ChildrenIterator;
      
      virtual void StartCallback () {};
      virtual void UpdateCallback () {};
      virtual void RenderCallback ( Simplex::Math::Matrix4* ) {};
      
    };
  }
}
#endif
