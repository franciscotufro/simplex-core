#ifndef __SIMPLEX_NODE_H__
#define __SIMPLEX_NODE_H__

#include <vector>
#include <string>

#include <Simplex/Core/Exception.h>

namespace Simplex
{
  namespace Core
  {
    class Node
    {
    public:
      typedef Exception NullParentException;
      typedef std::vector<Node*>::iterator ChildrenIterator;
      
      Node();
      ~Node();
      
      bool HasParent();
      Node* GetParent ();
      void SetParent (Node* _parent);
      void RemoveParent ();
      
      bool HasChild ( Node* object );
      Node* GetChildAt ( int index );
      int ChildCount ();
      
    private:
      Node* mParent;
      std::vector<Node*> mChildren;

      void InitializeParent();
      void ValidateParentNotNull ( Node* _parent );
      void AddChild ( Node* child );
      void RemoveChild ( Node* child );
      ChildrenIterator FindChild ( Node* child );
      
    };
  }
}
#endif
