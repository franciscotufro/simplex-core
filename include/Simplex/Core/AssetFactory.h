#ifndef __SIMPLEX_CORE_ASSET_FACTORY_H__
#define __SIMPLEX_CORE_ASSET_FACTORY_H__

#include <string>
#include <unordered_map>

namespace Simplex
{
  namespace Core
  {
    template < class AssetClass, class AssetDefinitionStruct >
    class AssetFactory
    {
    
    public:
      AssetFactory () :
        mDefinitions ( new std::unordered_map<std::string, AssetDefinitionStruct>() ) 
      {

      }

    	~AssetFactory ()
      {
        delete mDefinitions;
      }
    	
    	virtual void Define ( AssetDefinitionStruct assetDefinition ) 
      {
        (*mDefinitions)[assetDefinition.name] = assetDefinition;
      }

    	bool HasDefinitionFor ( std::string name )
      {
        return mDefinitions->find(name) != mDefinitions->end();
      }

      AssetClass* Create ( std::string definitionName )
      {
        AssetDefinitionStruct definition = (*mDefinitions)[definitionName];

        AssetClass* asset = new AssetClass();
        asset->SetDefinition ( definition );
        asset->ParseDefinition ();
        return asset;
      }

    private:
    	std::unordered_map<std::string, AssetDefinitionStruct>* mDefinitions;

    };
  }
}
#endif
