Module simplex-core
===

TODO:

  * Review error handling
  * Add ACTIVE | INACTIVE states to SceneObject for easy activation or deactivation.
  * Since I believe that SceneObjects in the SceneGraph tree will be near the root, it 
    makes sense to use breath-first search on them instead of the awful quickhack that
    I have now.
    
Refactoring:
  * Think a little bit about the name of this module. What does 'core' means?
  * Search for a better class name for Application.
  * Cleanup Object tests.

  


