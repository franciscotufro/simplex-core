#include <Simplex/Test.h>
#include <Simplex/Core/Adapter.h>
#include "AdapteeMock.h"

using namespace Simplex::Core;

TEST ( SimplexCoreAdapter, AcceptsAnAdaptee )
{
  AdapteeMock* adaptee = new AdapteeMock();
  Adapter adapter = Adapter();
  
  adapter.SetAdaptee( adaptee );
  ASSERT_EQ ( adaptee, adapter.GetAdaptee() );
}

TEST ( SimplexCoreAdapter, HasAdapteeReturnsFalseIfNoAdapteeSet )
{
  Adapter adapter = Adapter();
  ASSERT_FALSE ( adapter.HasAdaptee() );
}

TEST ( SimplexCoreAdapter, HasAdapteeReturnsTrueIfAdapteeSet )
{
  AdapteeMock* adaptee = new AdapteeMock();
  Adapter adapter = Adapter();
  
  adapter.SetAdaptee( adaptee );
  
  ASSERT_TRUE ( adapter.HasAdaptee() );
}

//TODO: TEST ALL BAD CASES WITH EXCEPTIONS AND ALL
