#include <Simplex/Test.h>
#include <Simplex/Core/Logger.h>

using namespace Simplex::Core;

std::stringstream stream;

void NewOutputStreamForLogger ()
{
  Logger::Instance()->SetOutputStream ( &stream );
}

std::string GetStringFromOutputStream ()
{
  
  std::stringstream* stream = dynamic_cast<std::stringstream*>(Logger::Instance()->GetOutputStream());
  return stream->str();
  return "ok";
}

void ResetStream ()
{
  stream.str(std::string());
}

TEST ( SimplexCoreLogger, LoggerIsASingleton )
{
  ASSERT_TRUE ( Logger::Instance () );
}

TEST ( SimplexCoreLogger, UsesStdOutByDefaultAsOutputStream )
{
  ASSERT_EQ ( std::cout, *Logger::Instance()->GetOutputStream () );
}

TEST ( SimplexCoreLogger, LoggerSetsOutputStream )
{
  NewOutputStreamForLogger ();
  ASSERT_EQ ( stream, *Logger::Instance()->GetOutputStream() );
}

TEST ( SimplexCoreLogger, ClearOutputStreamResetsOutputStreamToStdCout )
{
  NewOutputStreamForLogger ();
  ASSERT_EQ ( stream, *Logger::Instance()->GetOutputStream() );

  Logger::Instance()->ClearOutputStream();
  
  ASSERT_EQ ( std::cout, *Logger::Instance()->GetOutputStream () );
}

TEST ( SimplexCoreLogger, LogWritesStringToStreamOutput )
{
  NewOutputStreamForLogger ();
  std::string logString = "There is no escape—we pay for the violence of our ancestors.";
  Logger::Log(logString);
  ASSERT_EQ(logString + "\n", stream.str());

  ResetStream ();
}

TEST ( SimplexCoreLogger, LogWritesIntToStreamOutput )
{
  NewOutputStreamForLogger ();
  int value = 1337; 

  Logger::Log(value);

  ASSERT_EQ("1337\n", stream.str());
  ResetStream ();
}

TEST ( SimplexCoreLogger, LogWritesFloatToStreamOutput )
{
  NewOutputStreamForLogger ();
  float value = 1337.01; 

  Logger::Log(value);

  ASSERT_EQ("1337.01\n", stream.str());
  ResetStream ();
}