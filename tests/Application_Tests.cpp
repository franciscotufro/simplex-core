#include <Simplex/Test.h>
#include <Simplex/Core/Application.h>

using namespace Simplex::Core;

TEST ( SimplexCoreException, GetSceneGraphReturnsSceneGraph )
{
	SceneGraph* sceneGraph = Application().GetSceneGraph();
	ASSERT_TRUE ( sceneGraph );
}
