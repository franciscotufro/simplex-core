#include <Simplex/Test.h>
#include <Simplex/Core/Exception.h>

using namespace Simplex::Core;

TEST ( SimplexCoreException, CanInstanceAnExceptionWithGenericDescription )
{
  Exception exception = Exception();

  ASSERT_EQ("Generic Exception", exception.GetDescription() );
}

TEST ( SimplexCoreException, CanInstanceAnExceptionWithDescription )
{
  Exception exception = Exception("World end.");
  
  ASSERT_EQ("World end.", exception.GetDescription() );
}

TEST ( SimplexCoreException, OperatorLesserLesserOutputsDescription )
{
  std::stringstream stringStream;
  Exception exception = Exception("World end.");
  
  stringStream << exception;
  
  ASSERT_EQ("World end.", stringStream.str() );
}

TEST ( SimplexCoreException, ExceptionCanBeThrown )
{
  try
  {
    throw Exception("An Exception");
    ASSERT_TRUE(false);
  }
  catch ( Exception& e )
  {
    ASSERT_EQ( "An Exception", e.GetDescription() );
  }
}

TEST ( SimplexCoreException, StaticNullCheckThrowsOnNull )
{
  try
  {
    Exception exception = Exception("NullPointer");
    Exception::NullCheck ( NULL, exception );
    ASSERT_TRUE(false);
  }
  catch ( Exception& e )
  {
    ASSERT_EQ( "NullPointer", e.GetDescription() );
  }
}

TEST ( SimplexCoreException, StaticNullCheckWontThrowOnValidPointer )
{
  try
  {
    Exception exception = Exception();
    Exception::NullCheck ( &exception, exception );
    ASSERT_TRUE(true);
  }
  catch ( Exception& e )
  {
    ASSERT_TRUE( false );
  }
}