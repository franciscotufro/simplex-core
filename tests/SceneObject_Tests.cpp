#include <Simplex/Test.h>
#include <Simplex/Core/SceneObject.h>

using namespace Simplex::Core;
using namespace Simplex::Math;

TEST ( SimplexCoreSceneObject, InheritsFromNameable )
{
  SceneObject sceneObject = SceneObject ();
  
  ASSERT_TRUE( static_cast< Nameable* >( &sceneObject ) );
}

TEST ( SimplexCoreSceneObject, InheritsFromPositionable )
{
  SceneObject sceneObject = SceneObject ();
  
  ASSERT_TRUE( static_cast< Positionable* >( &sceneObject ) );
}