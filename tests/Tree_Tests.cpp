#include <Simplex/Test.h>
#include <Simplex/Core/Tree.h>
#include <Simplex/Core/Node.h>

using namespace Simplex::Core;

Tree tree;

//
// Mocks and Auxiliary Functions
// 

void RemoveNode( Node* node )
{
  tree.RemoveNode ( node );
  delete node;
}

Node* CreateAndAddNode ()
{
  Node* node = new Node ();
  
  tree.AddNode ( node );
  
  return node;

}

TEST ( SimplexCoreTree, AddsNode )
{
  Node* node = CreateAndAddNode ();
  
  ASSERT_TRUE ( tree.HasNode ( node ) );

  RemoveNode( node );
}

TEST ( SimplexCoreTree, RemovesNode )
{
  Node* node = CreateAndAddNode ();
  const Node* parent = node->GetParent ();

  tree.RemoveNode ( node );

  ASSERT_FALSE ( tree.HasNode ( node ) );
  ASSERT_FALSE ( node->HasParent () );

  delete node;
}

TEST ( SimplexCoreTree, RemoveNodeThrowsExceptionIfObjectNotFound )
{
  Node* node = new Node();

  try
  {
    tree.RemoveNode ( node );
    ASSERT_TRUE ( false );
  }
  catch ( Tree::ObjectNotFound& e )
  {
    ASSERT_EQ ( "Object not found.", e.GetDescription() );
  }

  delete node;
}

TEST ( SimplexCoreTree, GetRootNodeReturnsRootNode )
{
  Node* root = tree.GetRootNode ();

  try
  {
    ASSERT_FALSE ( root->GetParent () );
    ASSERT_TRUE ( false );
  }
  catch ( Node::NullParentException& e )
  {
    ASSERT_TRUE ( true );
  }
}