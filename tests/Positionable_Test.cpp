#include <Simplex/Test.h>
#include <Simplex/Core/Positionable.h>

using namespace Simplex::Core;
using namespace Simplex::Math;

TEST ( SimplexCorePositionable, GetPositionReturnsZeroAtStart )
{
  Positionable positionable = Positionable ();
  
  Vector3 position = positionable.GetPosition();
  
  ASSERT_EQ ( Vector3::ZERO,  position );
}

TEST ( SimplexCorePositionable, SetsPositionCorrectly )
{
  Positionable positionable = Positionable ();
  
  positionable.SetPosition ( Vector3::UP );
  Vector3 const position = positionable.GetPosition();
  
  ASSERT_EQ ( Vector3::UP, position );
}

TEST ( SimplexCorePositionable, GetScaleReturnsUnityScaleAtStart )
{
  Positionable positionable = Positionable ();
  
  Vector3 const scale = positionable.GetScale();
  
  ASSERT_EQ ( Vector3::UNIT_SCALE, scale );
}

TEST ( SimplexCorePositionable, SetsScaleCorrectly )
{
  Positionable positionable = Positionable ();

  positionable.SetScale ( Vector3::UP );
  Vector3 const scale = positionable.GetScale();

  ASSERT_EQ ( Vector3::UP, scale );
}

TEST ( SimplexCorePositionable, GetRotationReturnsIdentityAtStart )
{
  Positionable positionable = Positionable ();
  
  Quaternion const rotation = positionable.GetRotation();
  
  ASSERT_EQ ( Quaternion::IDENTITY, rotation );
}

TEST ( SimplexCorePositionable, SetsRotationCorrectly )
{
  Positionable positionable = Positionable ();

  positionable.SetRotation ( Quaternion::ZERO );
  Quaternion const rotation = positionable.GetRotation();

  ASSERT_EQ ( Quaternion::ZERO, rotation );
}

TEST ( SimplexCorePositionable, GetLocalTransformReturnsIdentityTransformOnStart )
{
  Positionable positionable = Positionable ();
  
  Matrix4 actual = positionable.GetLocalTransform ();
  
  ASSERT_EQ ( Matrix4::IDENTITY, actual );
  
}

TEST ( SimplexCorePositionable, GetTransformReturnsCorrectMatrixWhenMoved )
{
  Positionable positionable = Positionable ();
  
  positionable.SetPosition ( Vector3 (3, 2, 1) );
  positionable.SetScale ( Vector3 ( 3, 3, 3 ) );
  
  // We expect the affine transform to be
  //
  // | 3 0 0 3 |
  // | 0 3 0 2 |
  // | 0 0 3 1 |
  // | 0 0 0 1 |
  //
  // Since we moved it by (3,2,1) and
  // scaled it by (3,3,3)
  
  Matrix4 expected = Matrix4(
    3,0,0,3, 
    0,3,0,2,
    0,0,3,1,
    0,0,0,1);
  
  ASSERT_EQ (expected, positionable.GetLocalTransform () );
  
}

TEST ( SimplexCorePositionable, GetInverseTransformReturnsCorrectMatrixWhenMoved )
{
  Positionable positionable = Positionable ();
  
  positionable.SetPosition ( Vector3 (3, 2, 1) );
  positionable.SetScale ( Vector3 ( 3, 3, 3 ) );
  
  Matrix4 expected = Matrix4(
    3,0,0,3, 
    0,3,0,2,
    0,0,3,1,
    0,0,0,1).inverse();
  
  ASSERT_EQ (expected, positionable.GetLocalInverseTransform () );
  
}