#include <Simplex/Test.h>
#include <Simplex/Core/Nameable.h>

using namespace Simplex::Core;

TEST ( SimplexCoreNameable, SetsNameCorrectly )
{
  Nameable nameable;
  
  nameable.SetName ( "My Name" );
  
  ASSERT_EQ ( "My Name", nameable.GetName() );
}
