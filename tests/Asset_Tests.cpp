#include <Simplex/Test.h>
#include <Simplex/Core/Asset.h>

using namespace Simplex::Core;

TEST ( SimplexCoreAsset, InheritsFromNameable )
{
  Asset<AssetDefinition> asset;
  
  ASSERT_TRUE( static_cast< Nameable* >( &asset ) );
}

TEST ( SimplexCoreAsset, SetsDefinitionCorrectly )
{
  Asset<AssetDefinition> asset;
  AssetDefinition assetDefinition = { "My Asset", "asset.txt" };

  asset.SetDefinition ( assetDefinition );
  
  ASSERT_EQ ( assetDefinition.name, asset.GetDefinition ().name );
  ASSERT_EQ ( assetDefinition.filename, asset.GetDefinition ().filename );
}