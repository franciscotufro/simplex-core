#include <Simplex/Test.h>
#include <Simplex/Core/AssetFactory.h>
#include <Simplex/Core/Asset.h>
using namespace Simplex::Core;

AssetFactory<Asset<AssetDefinition>, AssetDefinition> factory;
AssetDefinition assetDefinition = { "TestAsset", "test_asset.blo" };

void DefineAsset ()
{
	factory.Define( assetDefinition );
}

TEST ( SimplexCoreAssetFactory, CanDefineAnAsset )
{
	DefineAsset ();

  ASSERT_TRUE ( factory.HasDefinitionFor ( "TestAsset" ) );
}

TEST ( SimplexCoreAssetFactory, CanCreateAnAssets )
{
	DefineAsset ();

	Asset<AssetDefinition> *asset = factory.Create( "TestAsset" );

  ASSERT_EQ ( assetDefinition.name, asset->GetDefinition().name );
}