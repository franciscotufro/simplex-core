#include <Simplex/Test.h>
#include <Simplex/Core/SceneGraph.h>
#include <Simplex/Core/SceneObject.h>

using namespace Simplex::Core;
using namespace Simplex::Math;

//
// Mocks and Auxiliary Functions
// 


SceneGraph sceneGraph;

class SceneObjectMock : public SceneObject
{
public:
  bool RenderCalled;
  bool UpdateCalled;
  bool StartCalled;
  Matrix4 CurrentTransform;
  
  SceneObjectMock ()
  {
    RenderCalled = false;
    UpdateCalled = false;
    StartCalled = false;
  }  
  void StartCallback () 
  {
    this->StartCalled = true;
  }
  
  void RenderCallback ( Matrix4* const currentTransform ) 
  {
    this->RenderCalled = true;
    this->CurrentTransform = *currentTransform;
  }
  
  void UpdateCallback ( )
  {
    this->UpdateCalled = true;
  }
};

void RemoveSceneObject( SceneObject* object )
{
  sceneGraph.RemoveSceneObject ( object );
  delete object;
}

SceneObjectMock* CreateAndAddSceneObjectMock ()
{
  SceneObjectMock* object = new SceneObjectMock ();
  
  sceneGraph.AddSceneObject ( object );
  
  return object;

}


//
// Tests Begin Here
// 

TEST ( SimplexCoreSceneGraph, AddsSceneObjects )
{
  SceneObjectMock* object = CreateAndAddSceneObjectMock ();
  
  ASSERT_TRUE ( sceneGraph.HasSceneObject ( object ) );

  RemoveSceneObject( object );
}

TEST ( SimplexCoreSceneGraph, RemovesSceneObjects )
{
  SceneObjectMock* object = CreateAndAddSceneObjectMock ();
  const SceneObject* parent = (SceneObject*)object->GetParent ();

  sceneGraph.RemoveSceneObject ( object );

  ASSERT_FALSE ( sceneGraph.HasSceneObject ( object ) );
  ASSERT_FALSE ( object->HasParent () );

  delete object;
}

TEST ( SimplexCoreSceneGraph, RemoveSceneObjectThrowsExceptionIfObjectNotFound )
{
  SceneObjectMock* object = new SceneObjectMock();

  try
  {
    sceneGraph.RemoveSceneObject ( object );
    ASSERT_TRUE ( false );
  }
  catch ( SceneGraph::ObjectNotFound& e )
  {
    ASSERT_EQ ( "Object not found.", e.GetDescription() );
  }

  delete object;
}

TEST ( SimplexCoreSceneGraph, RenderCallsChildrenRenderCallbackFunction )
{
  SceneObjectMock* object = CreateAndAddSceneObjectMock ();

  sceneGraph.Render ();

  ASSERT_TRUE ( object->RenderCalled );

  RemoveSceneObject( object );
}

TEST ( SimplexCoreSceneGraph, RenderPassesTheCorrectCurrentMatrixToCallbackFunction )
{
  SceneObjectMock* parent = CreateAndAddSceneObjectMock ();
  parent->SetName("Parent");
  SceneObjectMock* child = CreateAndAddSceneObjectMock ();
  child->SetName("Child");
  
  child->SetParent ( parent );

  parent->SetPosition ( Vector3 (3, 2, 1) );
  parent->SetScale ( Vector3 ( 3, 3, 3 ) );
    
  Matrix4 expectedOnChild = Matrix4(
    3,0,0,3, 
    0,3,0,2,
    0,0,3,1,
    0,0,0,1);

  sceneGraph.Render ();

  ASSERT_EQ ( expectedOnChild, child->CurrentTransform );
}


TEST ( SimplexCoreSceneGraph, UpdateCallsChildrenUpdateCallbackFunction )
{
  SceneObjectMock* object = CreateAndAddSceneObjectMock ();

  sceneGraph.Update ();

  ASSERT_TRUE ( object->UpdateCalled );

  RemoveSceneObject( object );
}

TEST ( SimplexCoreSceneGraph, StartTagsSceneGraphAsStarted )
{
  sceneGraph.Start ();

  ASSERT_TRUE ( sceneGraph.HasStarted () );

  sceneGraph.Stop ();
}

TEST ( SimplexCoreSceneGraph, StopTagsSceneGraphAsStopped )
{
  sceneGraph.Start ();
  sceneGraph.Stop ();
  ASSERT_FALSE ( sceneGraph.HasStarted () );
}

TEST ( SimplexCoreSceneGraph, AddingObjectRunsStartCallbackIfSceneGraphIsStarted )
{
  sceneGraph.Start ();

  SceneObjectMock* object = CreateAndAddSceneObjectMock ();
  
  ASSERT_TRUE ( object->StartCalled );

  RemoveSceneObject( object );
  sceneGraph.Stop();
}

TEST ( SimplexCoreSceneGraph, AddingObjectWontRunStartCallbackIfSceneGraphIsStopped )
{
  SceneObjectMock* object = CreateAndAddSceneObjectMock ();
  
  ASSERT_FALSE ( object->StartCalled );

  RemoveSceneObject( object );
  sceneGraph.Stop();
}

TEST ( SimplexCoreSceneGraph, StartRunsStartCallbackOnAllExistingObjects )
{
  SceneObjectMock* object = CreateAndAddSceneObjectMock ();

  sceneGraph.Start ();
  
  ASSERT_TRUE ( object->StartCalled );

  RemoveSceneObject( object );
  sceneGraph.Stop();
}
