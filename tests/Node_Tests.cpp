#include <Simplex/Test.h>
#include <Simplex/Core/Node.h>

using namespace Simplex::Core;

TEST ( SimplexCoreNode, EmptyParentOnStartThrowsExceptionOnGetParent )
{
  Node sceneObject = Node ();

  try
  {
    sceneObject.GetParent ();
    ASSERT_TRUE ( false );
  }
  catch ( Node::NullParentException& e )
  {
    ASSERT_EQ ( "Null parent for Node", e.GetDescription() );
  }

}

TEST ( SimplexCoreNode, SetParentAddsParent )
{
  Node child = Node ();
  Node parent = Node ();
  
  child.SetParent ( &parent );
  Node* actualParent = child.GetParent();
  
  ASSERT_EQ ( &parent, actualParent );
}

TEST ( SimplexCoreNode, HasParentReturnsFalseIfNoParent )
{
  Node sceneObject = Node ();
  
  ASSERT_FALSE ( sceneObject.HasParent() );
}

TEST ( SimplexCoreNode, HasParentReturnsTrueIfObjectHasAParent )
{
  Node child = Node ();
  Node parent = Node ();
  
  child.SetParent ( &parent );
  
  ASSERT_TRUE ( child.HasParent() );
}

TEST ( SimplexCoreNode, SetParentWithNullParentThrowsException )
{
  Node sceneObject = Node ();
  
  try
  {
    sceneObject.SetParent ( NULL );
    ASSERT_TRUE ( false );
  }
  catch ( Node::NullParentException& e )
  {
    ASSERT_EQ ( "Null parent for Node", e.GetDescription() );
  }
}

TEST ( SimplexCoreNode, SetParentDefinesChildRelation )
{
  Node* child = new Node ();
  Node* parent = new Node ();
  
  child->SetParent ( parent );
  
  ASSERT_TRUE ( parent->HasChild ( child ) );

  delete child;
  delete parent;
}

TEST ( SimplexCoreNode, SetParentRemovesOldChildRelation )
{
  Node* child = new Node ();
  Node* oldParent = new Node ();
  Node* parent = new Node ();
  
  child->SetParent ( oldParent );
  child->SetParent ( parent );
  
  ASSERT_FALSE ( oldParent->HasChild ( child ) );

  delete child;
  delete oldParent;
  delete parent;
}

TEST ( SimplexCoreNode, GetChildAtWorks )
{
  Node* child = new Node ();
  Node* parent = new Node ();
  
  child->SetParent ( parent );
  
  ASSERT_EQ ( child, parent->GetChildAt(0) );

  delete child;
  delete parent;
}

TEST ( SimplexCoreNode, RemoveParentClearsTheParent )
{
  Node child = Node ();
  Node parent = Node ();
  child.SetParent ( &parent );
  EXPECT_TRUE ( child.HasParent () );
  EXPECT_TRUE ( parent.HasChild ( &child ) );

  child.RemoveParent ();
  
  ASSERT_FALSE ( child.HasParent () );
  ASSERT_FALSE ( parent.HasChild ( &child ) );
}

TEST ( SimplexCoreNode, HasChildReturnsTrueWithChild )
{
  Node child = Node ();
  Node parent = Node ();

  child.SetParent ( &parent );

  ASSERT_TRUE ( parent.HasChild ( &child ) );
}

TEST ( SimplexCoreNode, HasChildReturnsFalseWithoutChild )
{
  Node child = Node ();
  Node parent = Node ();

  ASSERT_FALSE ( parent.HasChild ( &child ) );
}